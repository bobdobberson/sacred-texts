### Where to find me
  - on matrix (matrix.org): `@bobdobberson:matrix.org`
  - on IRC (efnet.org): `popebob`
  - on #fediverse: `@bobdobberson@dobbs.town`

I preach and teach on Facebook as `Pope Bob` when I can't keep myself from it, I do not use it as a one-on-one discussion platform.
