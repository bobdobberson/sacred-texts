### Premises about the Universe

- Humans survive long enough to create a near replica of the universe
- There are multiple replicas of the universe
- We are in a replica of the universe, not 'Universe Prime'
- We are composed of code
- We are all the same program, with different starting conditions
- This replica of the universe was written by a non-expert programmer
- There are bugs in the code that may be exploited
