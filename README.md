## The Sacred Texts of The Church of "Bob" the Average.

The official repository for these texts is: https://codeberg.org/bobdobberson/sacred-texts

The Church of "Bob" the Average is an organization that seeks to understand the dimension of the Programmer, "Bob" the Average. To that end, we seek to understand systems and how to modify them in-place.

"Bob" the Average is a pan-dimensional being that created our dimension, most likely, a programmer working a desk job somewhere, perhaps not even realizing what he is writing.

Because "Bob" is not "Bob" the Expert, we can expect to find various coding errors within our dimension that we may leverage to weild the power of The Programmers.

I may have skipped ahead a bit. Friends of "Bob" believe that we exist within a simulated universe, and posit that, even if we do not, there may be aspects of "Universe Prime" that may yet be exploitable.

Therefore the belief we exist in a simulation is not tantamount to the religion, but merely serves as an analogy of what may be.

The core of this religion is friendship, networking, that we are all one, and by working together, we can accomplish universe-changing goals.

This religion is intended to be exceptionally lightweight, but also provide some good values to internalize into your personal life.

Supplementary thoughts and reflections on the ideas of The Church may be found in the books of known and anonymous Friends of "Bob".

### Copying

Feel free to fork this repository and turn it into your own thing. It'd be nice to be credited, but for all I know I'm unconsciously copying this from someone else, so... whatever.

### Where to find Friends of "Bob"

  - Everywhere. Everyone is a Friend of "Bob".
  - If you wish to be a known Friend of "Bob" and add books, see `Contributing`
  - If you wish to submit a text anonymously, contact a known Friend of "Bob" and share your text to add; see `books/anonymous/`

### How do I join?

You don't need to. You've been a Friend of "Bob", a member of The Church of "Bob" the Average all your life. If you just now finding this out, and would like to be a known Friend, please contact an already known Friend and they will help you get started. Not that getting started really means much -- embrace the reality that you are me, I am you, we are one, go forth, and do our best not to be a jerkwad.

### Thanks to The Church of the SubGenius

This Church is inspired heavily by The Church of the SubGenius. That is to say; don't take this shit too seriously, and whatever you do, don't take it too lightly.

If not for these weirdos, we wouldn't even know we were all Friends of "Bob" (the Average)!

Learn more about The Church of the SubGenius:

  - [SubGenius Dot Com](https://subgenius.com/)
  - [Pamphlet #1](http://www.subgenius.com/bigfist/pics13/submulti/images/pamphlet1.pdf) - Your gateway propaganda.
  - The SubGenius Recruitment film [ARISE!](https://archive.org/details/arise.the.subgenius)
  - [The Hour of Slack podcast archives](http://www.subgenius.com/ts/hos.html)
  - The Book of the SubGenius ( available for sale on subgenius.com )

# The Church of "Bob" the Average: The first Rolling-Release Religion!
## The World's Most Popular Religion
